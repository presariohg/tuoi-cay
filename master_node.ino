#include <RF24.h>

#include "SIM900.h"

#include "sms.h"

#include <RF24Network.h>

#include <SPI.h>

#include <SoftwareSerial.h>

#include <avr/interrupt.h>


//RF communication
RF24 radio(9, 10); // nRF24L01 (CE,CSN)
RF24Network network(radio); // Include the radio in the network
const uint16_t this_node = 00; // Address of this node in Octal format ( 04,031, etc)
const uint16_t node01 = 01;
const uint16_t node02 = 02;
const uint16_t node03 = 03;
const uint16_t node04 = 04;
unsigned char nodeID;
unsigned char modeValue;
unsigned char soilValue;
unsigned char tempValue;
unsigned char lumenValue;

struct payload {
    unsigned char node;
    unsigned char mode;
    unsigned char soil;
    unsigned char temp;
    unsigned char light;
};
SMSGSM sms;
boolean started = false;
char smstext[160];
char number[20];
const unsigned long interval = 2000; //ms  // How often to pump
unsigned long last_pump; //when did we last pump
boolean opMode = false;
#define bom 8
#define relay_1 A1
#define relay_2 A2
#define relay_3 A3
#define relay_4 A4

int * relay = new int[5]; //mang relay chua tat ca cac relay, khong dung relay[0]. relay[1] = relay_1, .. relay[4] = relay_4 cho de doc

void setup(void) {
    relay[1] = relay_1;
    relay[2] = relay_2;
    relay[3] = relay_3;
    relay[4] = relay_4;

    
    Serial.begin(9600);
    if (gsm.begin(2400)) {
        started = true;
    }
    pinMode(bom, OUTPUT);
    pinMode(relay_1, OUTPUT);
    pinMode(relay_2, OUTPUT);
    pinMode(relay_3, OUTPUT);
    pinMode(relay_4, OUTPUT);
    digitalWrite(bom, LOW);
    digitalWrite(relay_1, LOW);
    digitalWrite(relay_2, LOW);
    digitalWrite(relay_3, LOW);
    digitalWrite(relay_4, LOW);

    SPI.begin();
    radio.begin();
    network.begin(90, this_node); //(channel, node address)
    radio.setDataRate(RF24_2MBPS);
    cli(); // tắt ngắt toàn cục
    /* Reset Timer/Counter1 */
    TCCR1A = 0;
    TCCR1B = 0;
    TIMSK1 = 0;
    /* Setup Timer/Counter1 */
    TCCR1B |= (1 << CS11) | (1 << CS10); // prescale = 64
    TCNT1 = 0;
    TIMSK1 = (1 << TOIE1); // Overflow interrupt enable 
    sei();
}

void loop(void) {
    network.update();
    read_sms();
    receiver();
    send();
}
void send() {
    RF24NetworkHeader header1(node01); // (Address where the data is going)
    bool ok = network.write(header1, & opMode, sizeof(opMode)); // Send the data
    RF24NetworkHeader header2(node02); // (Address where the data is going)
    bool ok_1 = network.write(header2, & opMode, sizeof(opMode)); // Send the data
    RF24NetworkHeader header3(node03); // (Address where the data is going)
    bool ok_2 = network.write(header3, & opMode, sizeof(opMode)); // Send the data
    RF24NetworkHeader header4(node04); // (Address where the data is going)
    bool ok_3 = network.write(header4, & opMode, sizeof(opMode)); // Send the data
}

void auto_proccess() {
    if ((nodeID == 1) && (modeValue == 1) && (soilValue <= 20) && (tempValue <= 25)) {
        unsigned long now = millis();
        if (now - last_pump >= interval) {
            last_pump = now;
            digitalWrite(relay_1, HIGH);
            digitalWrite(relay_2, LOW);
            digitalWrite(relay_3, LOW);
            digitalWrite(relay_4, LOW);
            delay(200);
            digitalWrite(bom, HIGH);
        }
    } else if ((nodeID == 2) && (modeValue == 1) && (soilValue <= 20) && (tempValue <= 25)) {
        unsigned long now = millis();
        if (now - last_pump >= interval) {
            last_pump = now;
            digitalWrite(relay_1, LOW);
            digitalWrite(relay_2, HIGH);
            digitalWrite(relay_3, LOW);
            digitalWrite(relay_4, LOW);
            delay(200);
            digitalWrite(bom, HIGH);
        }
    } else if ((nodeID == 3) && (modeValue == 1) && (soilValue <= 20) && (tempValue <= 25)) {
        unsigned long now = millis();
        if (now - last_pump >= interval) {
            last_pump = now;
            digitalWrite(relay_1, LOW);
            digitalWrite(relay_2, LOW);
            digitalWrite(relay_3, HIGH);
            digitalWrite(relay_4, LOW);
            delay(200);
            digitalWrite(bom, HIGH);
        }
    } else if ((nodeID == 4) && (modeValue == 1) && (soilValue <= 20) && (tempValue <= 25)) {
        unsigned long now = millis();
        if (now - last_pump >= interval) {
            last_pump = now;
            digitalWrite(relay_1, LOW);
            digitalWrite(relay_2, LOW);
            digitalWrite(relay_3, LOW);
            digitalWrite(relay_4, HIGH);
            delay(200);
            digitalWrite(bom, HIGH);
        }
    } else {
        digitalWrite(bom, LOW);
        digitalWrite(relay_1, LOW);
        digitalWrite(relay_2, LOW);
        digitalWrite(relay_3, LOW);
        digitalWrite(relay_4, LOW);
    }
}
void manual_process() { // lan luot bat tat 4 voi
    for (int voi = 1; voi <= 4; voi++) 
        tuoi(voi);
}

void tuoi(int voi) { // tuoi(1) = tuoi voi 1, tuoi(2) = tuoi voi 2, ...
    // trang thai 1 voi khong phu thuoc vao cac voi con lai, vd co the mo voi 1 trong khi voi 2 dang tuoi, khong anh huong
    digitalWrite(relay[voi], HIGH); // mo voi mong muon
    
    // bat voi mong muon trong 15 phut, nho phai x PHUT! Quan trong! Neu chi viet 15 thi khong hoat dong, phai la 15 x PHUT!
    timer_voi_p[voi] = 15 * PHUT; 

    if (digitalRead(bom) == LOW) { // neu may bom dang tat
        delay(1000);               // delay 1s doi vo mo ra
        digitalWrite(bom, HIGH);   // bat may bom
    }


    // digitalWrite(bom, LOW); // tat may bom

    // for (int i = 1; i <= 4; i++) {
    //     digitalWrite(relay[i], LOW); // tat tat ca 4 relay
    // }

    // digitalWrite(relay[voi], HIGH) // bat relay mong muon

    // delay(1000); // doi 1s de voi mo

    // digitalWrite(bom, HIGH);

    // delay(3000);

    // for (int i = 1; i <= 4; i++) {
    //     digitalWrite(relay[i], LOW); // tat tat ca 4 relay
    // }
}


void tat(int voi) { // tat 1 voi nhat dinh: tat(1) = tat voi 1,...
    // neu chua tat het thi khong tat may bom, neu muon tat ca 4 voi thi phai tat may bom truoc
    bool tat_het = true;

    for (int i = 1; i <= 4; i++) {
        if ((digitalRead(relay[i]) == HIGH) && // neu co 1 voi con dang mo
            (i != voi)) {                      // .. va voi nay khong phai voi sap bi tat 
            tat_het = false;                   // thi tat_het = false; khong tat may bom 
            break;
        }
    }

    if (tat_het)
        digitalWrite(bom, LOW); // muon tat het voi thi tat may bom truoc

    delay(1000); //delay 1s doi may bom tat

    digitalWrite(relay[voi], LOW); // tat voi muon tat


}

void receiver() {
    while (network.available()) { // Is there any incoming data?
        RF24NetworkHeader header;
        payload dataReceive;
        network.read(header, & dataReceive, sizeof(dataReceive)); // Read the incoming data
        nodeID = dataReceive.node;
        modeValue = dataReceive.mode;
        soilValue = dataReceive.soil;
        tempValue = dataReceive.temp;
        lumenValue = dataReceive.light;
        Serial.print("Data from node: ");
        Serial.println(dataReceive.node);
        if (modeValue == 0) {
            Serial.println("Manual mode");
            manual_process();
        } else {
            Serial.println("Auto mode");
            Serial.print("Soil: ");
            Serial.print(soilValue);
            Serial.print(",  Temp: ");
            Serial.print(tempValue);
            Serial.print(",  Lumen: ");
            Serial.println(lumenValue);
            auto_proccess();
        }
    }
}
void read_sms() {
    unsigned char pos;
    pos = sms.IsSMSPresent(SMS_UNREAD);
    if (pos) {
        if (sms.GetSMS(pos, number, smstext, 160)) {
            Serial.print("So dien thoại: ");
            Serial.println(number);
            Serial.print("Noi dung tin nhan: ");
            Serial.println(smstext);
            if (strcmp(smstext, "*auto#") == 0) { //so sánh 2 chuỗi
                opMode = 1;
            } else if (strcmp(smstext, "*manual#") == 0) {
                opMode = 0;
            }
        }
        sms.DeleteSMS(byte(pos)); //xóa sms vừa nhận
    }
    delay(1000);
}